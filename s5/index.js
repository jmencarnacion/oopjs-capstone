/*
Customer class
email property - string
cart property - instance of Cart class ex property1: new Example()
orders property - empty array
checkOut() - if the Cart is not empty, push an object to the orders array with the structure { products: cart content, totalAmount:cart total } 
*/

class Customer {
  constructor(email){
    this.email = email
    this.cart = new Cart()
    this.orders = []
  }
  checkOut() {
    if (this.cart.contents.length > 0) {
      this.orders.push({
        products: this.cart.contents,
        totalAmount: this.cart.computeTotal().totalAmount
      })
    }
    return this
  }
}

/*
Cart class
contents property - empty array
totalAmount - number
addToCart() - accepts a Product instance and a quantity number as arguments,
updateProductQuantity() - takes in a string used to find a product in a cart by name, and a new quantity.replaces the quantity of the found product in the cart with the new value
clear Cart Contents() empties the cart contents
computeTotal() - iterates over every product in the cart, multiplying product price with their respective quantities.Sums up all result and sets value as totalAmount
*/
class Cart {
  constructor(email){
    this.contents = []
    this.totalAmount = 0
  }

  addToCart(product, quantity){
    this.contents.push({
      product:product,
      quantity:quantity
    })
    return this
  }

  showCartContents(){
    console.log(this.contents)
    return this
  }

  updateProductQuantity(name, quantity){
    this.contents.find(item => item.product.name === name).quantity = quantity

    return this
  }

  clearCartContents(){
    this.contents = []

    return this
  }
  
  computeTotal(){
    this. totalAmount = 0
    if (this.contents.length > 0) {
      this.contents.forEach(item => {
        this.totalAmount = this.totalAmount + (item.product.price * item.quantity)
      })
    }
    return this
  }
}


/* 
Product class
name property - string
price property - number
isActive property - Boolean defaults to true
archive() - will set isActive to false if it is true to begin with
updatePrice() - replaces product price with passed in numerical value
*/

class Product {
  constructor (name, price) {
    this.name = name
    this.price = price
    this.isActive = true
  }
  archive() {
    if (this.isActive === true) {
      this.isActive= false
    }
    return this
  }
  updatePrice(newPrice){
    this.price = newPrice
    return this
  }
}

//test:

// let john = new Customer('john@mail.com')

// let productA = new Product('soap', 9.99)
// let productB = new Product('shampoo', 12.99)
// let productC = new Product('toothpaste', 14.99)
// let productD = new Product('toothbrush', 4.99)

// john.cart.addToCart(productA, 3)
// john.cart.addToCart(productB, 2)

// john.cart.updateProductQuantity('soap', 5)
// john.cart.clearCartContents()
// john.cart.showCartContents()
// john.cart.computeTotal()

// john.checkOut()
// console.log(john)
